FROM openjdk:8-jdk-alpine
EXPOSE 8888
COPY ./target/Product-Service-0.0.1-SNAPSHOT.jar /Product-Service-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","Product-Service-0.0.1-SNAPSHOT.jar"]
