package ma.jad.jr.repository;

import ma.jad.jr.model.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface PictureRepository extends JpaRepository<Picture,Long> {
    Optional<Picture> findByName(String namePicture);
}
