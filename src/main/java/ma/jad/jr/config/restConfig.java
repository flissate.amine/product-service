package ma.jad.jr.config;

import ma.jad.jr.model.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
public class restConfig implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
        config.exposeIdsFor(Product.class);
        cors.addMapping("/**").allowedOrigins("/localhost:4200");
        cors.addMapping("http://localhost:8887/products/saveProduct").allowedOrigins("/http://localhost:4200/products/save");
        cors.addMapping("http://localhost:8887/products/image/upload").allowedOrigins("/http://localhost:4200/products/image/upload");
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipart = new CommonsMultipartResolver();
        multipart.setMaxUploadSize(3 * 1024 * 1024); return multipart;
    }
}
