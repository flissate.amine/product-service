package ma.jad.jr.controller;

import lombok.extern.slf4j.Slf4j;
import ma.jad.jr.model.dto.ProductRequestDTO;
import ma.jad.jr.model.Picture;
import ma.jad.jr.model.Product;
import ma.jad.jr.repository.PictureRepository;
import ma.jad.jr.repository.ProductRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@CrossOrigin("*")
@Slf4j
@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductRepository productRepository;
    private final PictureRepository pictureRepository;

    public static final String MEDIATYPE_IMAGE = MediaType.IMAGE_JPEG_VALUE;
    public static final String MEDIATYPE_JSON = MediaType.APPLICATION_JSON_VALUE;
    public static final String MEDIATYPE_MULTIPART = MediaType.MULTIPART_FORM_DATA_VALUE;

    public static final String IMAGE_UPLOAD = "/image/upload";
    public static final String GET_IMAGE = "/image/get";

    public ProductController(ProductRepository productRepository, PictureRepository pictureRepository) {
        this.productRepository = productRepository;
        this.pictureRepository = pictureRepository;
    }

    @PostMapping(value = IMAGE_UPLOAD, consumes = MEDIATYPE_MULTIPART)
    public void uploadImage(@RequestParam("imageFile") MultipartFile file) {
        try {
            if (!file.isEmpty()) {
                log.info("incoming request to upload product picture");
                Picture image = new Picture(null, file.getName(), file.getContentType(), compressBytes(file.getBytes()));
                log.info("create new picture");
                pictureRepository.saveAndFlush(image);
                log.info("this image was correctly saved into DB");
            }
            if (file.isEmpty()) {
                log.error("ERROR FILE IMPORTATION : file cant be empty !");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            log.info("somthing gone wrong , cant upload this pic");
        }
    }

    @GetMapping(path = {GET_IMAGE + "/{name}"})
    @ResponseBody
    public Picture getImage(@PathVariable("name") @NotNull String name) {
        log.info("incoming request to fetch image with name {} from DB", name);
        if (name.isEmpty()) {
            log.error("path variable cant be empty or null");
        }
        final Optional<Picture> retrievedImage = pictureRepository.findByName(name);
        if (retrievedImage.isPresent()) {
            Picture finalPic = retrievedImage.get();
            return new Picture(finalPic.getId(), finalPic.getName(), finalPic.getType(), decompressBytes(finalPic.getPicByte()));
        }
        log.warn("cant find this image with name {} ", name);
        return null;
    }

    @PostMapping(path = "/saveProduct", consumes = MEDIATYPE_JSON)
    public void saveProduct(@RequestBody ProductRequestDTO product) {
        try {
            if (product != null) {
                log.info("Creation new Product");
                Product productToSave = new Product();
                productToSave.setNameProduct(product.getNameProduct());
                productToSave.setPrice(product.getPrice());
                productToSave.setAvailable(product.isAvailable());
                productToSave.setDescription(product.getDescription());
                log.info("incoming request to save product with ID: {}", productToSave.getUuid());
                productRepository.save(productToSave);
                log.info("this product {} was correctly saved", productToSave.getNameProduct());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("something goes wrong , cant save this product");
        }
    }

    @GetMapping(path = "/{id}", consumes = MEDIATYPE_JSON)
    @ResponseBody
    public ResponseEntity<Product> getProductById(@PathVariable int id) throws Exception {
        log.info("incoming request to get Product with ID :" + id);
        if (id == 0) {
            log.error("Product ID cannot be equal to 0");
        }
        return ResponseEntity.ok().body(productRepository.findById(id).orElseThrow(Exception::new));
    }

    @GetMapping(path = "/all", produces = MEDIATYPE_JSON)
    @ResponseBody
    public ResponseEntity<List<Product>> getAllProducts() {
        log.info("incoming request to get all PRODUCTS IN THE STORE");
        return ResponseEntity.ok().body(productRepository.findAll());
    }

    public static byte[] compressBytes(byte[] data) {
        log.info("request info : compress image product before saving into DB");
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Compressed Image Byte Size - " + outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }

    public static byte[] decompressBytes(byte[] data) {
        log.info("request info : Decompress image product before sending to client");
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException | DataFormatException ioe) {
            ioe.printStackTrace();
        }
        return outputStream.toByteArray();
    }
}
