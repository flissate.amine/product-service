package ma.jad.jr;

import lombok.extern.slf4j.Slf4j;
import ma.jad.jr.model.Product;
import ma.jad.jr.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.UUID;
@Slf4j
@SpringBootApplication
public class ProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner start(ProductRepository productRepository ) {
        return args -> {
            UUID uuid = UUID.randomUUID();
            Product p1 = new Product(uuid.toString(),"paracetamol", "comprimé pour mal de téte",null, 45.00, true, 1);
            Product p2 = new Product(uuid.toString(), "sirop", "contre les toux et grippe",null, 100.00,true,2);
            Product p3 = new Product(uuid.toString(), "Doliprane", "comprimé 500mg",null, 89.00,true,1);
            Product p4 = new Product(uuid.toString(), "ventoline", "maladie respiratoire",null, 320.00,true,1);
            Product p5 = new Product(uuid.toString(), "masque medical", "COVID19",null, 320.00,true,1);
            Product p6 = new Product("", "Lasilix", "traitement du coeur",null, 250.00,true,3);
            Product p7 = new Product("", "Rennie", "brulure d'estomac",null, 23.00,true,1);
            Product p8 = new Product("", "Gel desinfectant", "CVD19",null, 32.00,false,4);
            Product p9 = new Product("", "Gants medicaux", "COVID",null, 25.00,false,1);
            Product p10 = new Product("", "Bain de bouche", "Dentition et soins",null, 89.00,true,1);
            productRepository.save(p1);
            productRepository.save(p2);
            productRepository.save(p3);
            productRepository.save(p4);
            productRepository.save(p5);
            productRepository.save(p6);
            productRepository.save(p7);
            productRepository.save(p8);
            productRepository.save(p9);
            productRepository.save(p10);

            log.info("list produits enregistrés" + p1 + p2 + p3 + p4);

        };
    }
}
