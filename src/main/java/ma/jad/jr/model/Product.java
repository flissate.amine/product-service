package ma.jad.jr.model;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    @Basic(optional = false)
    @Column(name = "id",unique=true, nullable = false)
    String uuid = UUID.randomUUID().toString();
    private String nameProduct;
    private String description;
    @Lob
    private byte[] picture;
    private double price;
    private boolean isAvailable;
    @Transient
    private int quantity = 1;
}
