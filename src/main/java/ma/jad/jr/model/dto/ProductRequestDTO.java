package ma.jad.jr.model.dto;

import lombok.Data;

import javax.persistence.Transient;

@Data
public class ProductRequestDTO {
    private String nameProduct;
    private String description;
    private double price;
    private boolean isAvailable;
    @Transient
    private int quantity = 1;
}
